-module(comments_handler).

-export([init/3]).
-export([allowed_methods/2]).
-export([content_types_accepted/2]).
-export([create_comment/2]).

init(_Transport, _Req, []) ->
    {upgrade, protocol, cowboy_rest}.

allowed_methods(Req, State) ->
    {[<<"POST">>], Req, State}.

content_types_accepted(Req, State) ->
    {[{{<<"application">>, <<"json">>, [{<<"charset">>, <<"utf-8">>}]},
        create_comment}], Req, State}.

create_comment(Req, State) ->
    {ok, CommentJSON, Req1} = cowboy_req:body(Req),
    [{<<"postId">>, PostId}, {<<"author">>, Author}, {<<"text">>, Text}] = jsx:decode(CommentJSON),
    lager:info("Comment received to post #~p from user \"~p\" with text \"~p\"", [PostId, Author, Text]),

    timer:sleep(2000), %% To simulate network latency

    {true, Req1, State}.
