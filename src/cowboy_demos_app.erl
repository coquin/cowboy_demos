-module(cowboy_demos_app).
-behaviour(application).

%% Application callbacks
-export([
    start/2,
    stop/1
]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    Dispatch = cowboy_router:compile([
        {'_', [
            %% Static handlers
            {"/css/[...]", cowboy_static, {priv_dir, cowboy_demos, "css",
                [{mimetypes, cow_mimetypes, web}]}},
            {"/js/[...]", cowboy_static, {priv_dir, cowboy_demos, "js",
                [{mimetypes, cow_mimetypes, web}]}},
            {"/img/[...]", cowboy_static, {priv_dir, cowboy_demos, "img",
                [{mimetypes, cow_mimetypes, web}]}},


            %% Routing
            {"/registration", registration_handler,
                [{mimetypes, {<<"text">>, <<"html">>, []}}]},
            {"/", index_handler,
                [{mimetypes, {<<"text">>, <<"html">>, []}}]},

            %% API routing
            {"/api/posts", posts_handler, []},
            {"/api/posts/:post_id/comments", comments_handler, []},

            %% EventSource
            {"/eventsource", eventsource_handler, []},

            %% WebSocket
            {"/websocket", ws_handler, []},

            {'_', notfound_handler,
                [{mimetypes, {<<"text">>, <<"html">>, []}}]}
        ]}
    ]),
    Port = port(),
    {ok, _} = cowboy:start_http(http_listener, 100, [{port, Port}], [
        {env, [{dispatch, Dispatch}]},
        {middlewares, [cowboy_router, cowboy_handler]}
    ]),

    lager:info("Web server started at http://localhost:~p", [Port]),
    cowboy_demos_sup:start_link().

stop(_State) ->
    ok.

%% ===================================================================
%% Internal functions
%% ===================================================================

port() ->
    case os:getenv("PORT") of
        false ->
            {ok, Port} = application:get_env(http_port),
            Port;
        Other ->
            list_to_integer(Other)
    end.
