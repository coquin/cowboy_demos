-module(eventsource_handler).

-export([init/3]).
-export([info/3]).
-export([terminate/3]).

init(_Transport, Req, []) ->
    Headers = [{<<"content-type">>, <<"text/event-stream">>}],
    {ok, Req1} = cowboy_req:chunked_reply(200, Headers, Req),
    erlang:send_after(1000, self(), {message, "Tick"}),
    {loop, Req1, undefined, 5000}.

info({message, Msg}, Req, State) ->
    ok = cowboy_req:chunk(["id: ", "100500", "\ndata: ", Msg, "\n\n"], Req),
    lager:info("EventSource tick", []),
    erlang:send_after(1000, self(), {message, "Tick"}),
    {loop, Req, State}.

terminate(_Reason, _Req, _State) ->
    ok.
