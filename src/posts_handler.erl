-module(posts_handler).

-export([init/3]).
-export([content_types_provided/2]).
-export([posts_json/2]).

init(_Transport, _Req, []) ->
    {upgrade, protocol, cowboy_rest}.

content_types_provided(Req, State) ->
    {[
        {<<"application/json">>, posts_json}
    ], Req, State}.

posts_json(Req, State) ->
    Posts = [
        [
            {<<"title">>, <<"Ajax (mythology)">>},
            {<<"body">>, <<"<strong>Ajax</strong> or <strong>Aias</strong> (/ˈeɪdʒæks/ or /ˈaɪ.əs/; Ancient Greek: Αἴας, gen. Αἴαντος) was a mythological Greek hero, the son of King Telamon and Periboea, and the half-brother of Teucer.[1] He plays an important role in Homer's <em>Iliad</em> and in the Epic Cycle, a series of epic poems about the Trojan War. To distinguish him from Ajax, son of Oileus (Ajax the Lesser), he is called &quot;<strong>Telamonian Ajax</strong>,&quot; &quot;<strong>Greater Ajax</strong>,&quot; or &quot;<strong>Ajax</strong> the Great&quot;. In Etruscan mythology, he is known as Aivas Tlamunus."/utf8>>},
            {<<"comments">>, [
                [
                     {<<"author">>, <<"guest">>},
                     {<<"text">>, <<"Thrill is gone">>}
                ],
                [
                     {<<"author">>, <<"nyancat">>},
                     {<<"text">>, <<"Meow meow">>}
                ]
             ]
            }
        ]
    ],

    timer:sleep(2000), %% To simulate network latency

    {jsx:encode(Posts), Req, State}.
