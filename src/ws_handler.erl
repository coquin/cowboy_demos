%% 
%% Based on
%% http://blog.dberg.org/2012/04/using-gproc-and-cowboy-to-pass-messages.html
%% http://erlang.org/pipermail/erlang-questions/2012-March/065220.html
%%
-module(ws_handler).
-behaviour(cowboy_websocket_handler).

-export([init/3]).
-export([websocket_init/3]).
-export([websocket_handle/3]).
-export([websocket_info/3]).
-export([websocket_terminate/3]).

-define(WSKey, {pubsub, wsbroadcast}).

init({tcp, http}, _Req, _Opts) ->
    {upgrade, protocol, cowboy_websocket}.

websocket_init(_TransportName, Req, _Opts) ->
    gproc:reg({p, l, ?WSKey}),
    {ok, Req, undefined_state}.

websocket_handle({text, Msg}, Req, State) ->
    JSON = jsx:decode(Msg),
    log_message(JSON),
    gproc:send({p, l, ?WSKey}, {self(), ?WSKey, Msg}),
    {ok, Req, State}; % Simply return message as is
websocket_handle(_Data, Req, State) ->
    {ok, Req, State}.

websocket_info({_PID, ?WSKey, Msg}, Req, State) ->
    {reply, {text, Msg}, Req, State};
websocket_info({timeout, _Ref, Msg}, Req, State) ->
    {reply, {text, Msg}, Req, State};
websocket_info(_Info, Req, State) ->
    {ok, Req, State}.

websocket_terminate(_Reason, _Req, _State) ->
    ok.

%% ===================================================================
%% Internal functions
%% ===================================================================

log_message([{<<"type">>, <<"notify">>}, {<<"user">>, User}, {<<"message">>, Message}]) ->
    lager:info("WebSocket system message received from user \"~p\": \"~p\"", [User, Message]);

log_message([{<<"type">>, <<"message">>}, {<<"user">>, User}, {<<"message">>, Message}]) ->
    lager:info("WebSocket chat message received from user \"~p\": \"~p\"", [User, Message]).
