Cowboy demos
============

A pack of cowbow webserver demos (registration, authorisation, session management, REST api, web sockets, etc.)

Usage
-----

```
make deps
make compile
make run
```

Web server will start at `http://localhost:8008`.

Useful links
------------

<http://habrahabr.ru/post/173595/>
<http://levgem.livejournal.com/409755.html>
<https://spqr.eecs.umich.edu/papers/webauth-tr.pdf>


WebSocket chat implementation:

<http://erlang.org/pipermail/erlang-questions/2012-March/065220.html>
<http://blog.dberg.org/2012/04/using-gproc-and-cowboy-to-pass-messages.html>