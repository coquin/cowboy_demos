angular.module("App", ["ngSanitize", "ngResource"])
    // TODO: place real user data here when authorization will be implemented
    .value("user", {
        name: "guest"
    })

    .service("Ticks", function() {
        var source;

        if (!!window.EventSource) {
            return {
                start: function(onStart, onStop, onMessage, onError) {
                    source = new EventSource("/eventsource");

                    if (onStart) {
                        source.addEventListener("open", onStart, false);
                    }

                    if (onMessage) {
                        source.addEventListener("message", function(event) {
                            onMessage(event.data);
                        }, false);
                    }

                    source.addEventListener("error", function(event) {
                        if (event.eventPhase == EventSource.CLOSED && onStop) {
                            // onStop();
                        } else if (onError) {
                            onError();
                        }
                    }, false);
                },

                stop: function() {
                    source && source.close();
                    source = null;
                }
            };
        } else {
            return null;
        }
    })

    .service("Chat", function(user) {
        var webSocket;

        if ("WebSocket" in window) {
            return {
                join: function(options) {
                    options = options || {};
                    this.options = options;

                    webSocket = new WebSocket("ws://" + window.location.host + "/websocket");
                    webSocket.onopen = function() {
                        webSocket.send(JSON.stringify({
                            type: "notify",
                            user: user.name,
                            message: "has joined the chat room."
                        }));
                        options.onJoin && options.onJoin();
                    };

                    webSocket.onclose = options.onClose;
                    webSocket.onmessage = options.onMessage;
                },

                leave: function() {
                    webSocket.send(JSON.stringify({
                        type: "notify",
                        user: user.name,
                        message: "has left the chat room."
                    }));

                    webSocket.close();
                    webSocket = null;
                    this.options.onLeave && this.options.onLeave();
                },

                sendMsg: function(message) {
                    var messageObj;

                    if (webSocket.readyState == webSocket.OPEN) {
                        messageObj = {
                            type: "message",
                            user: user.name,
                            message: message
                        };

                        webSocket.send(JSON.stringify(messageObj));
                    }
                }
            };
        } else {
            return null;
        }
    })

    .factory("Posts", function($resource) {
        return $resource("/api/posts", null, {
            get: {
                method: "GET",
                isArray: true
            }
        });
    })

    .factory("Comments", function($resource) {
        return $resource("/api/posts/:post_id/comments", { post_id: "@postId" });
    })

    .controller("AjaxSectionCtrl", function($scope, user, Posts, Comments) {
        $scope.post = null;
        $scope.comment = null;

        $scope.loadPost = function() {
            $scope.isPostLoading = true;

            Posts.get().$promise.then(function(posts) {
                $scope.post = posts[0];
                $scope.isPostLoading = false;
            });
        };

        $scope.postComment = function() {
            if ($scope.comment) {
                var comment = new Comments({
                    postId: 100500
                });

                comment.author = user.name;
                comment.text = $scope.comment;

                $scope.isCommentPosting = true;

                comment.$save(function() {
                    $scope.post.comments.unshift({
                        author: user.name,
                        text: $scope.comment
                    });

                    $scope.isCommentPosting = false;
                    $scope.comment = null;
                });
            }
        };
    })

    .controller("EventSourceSectionCtrl", function($scope, Ticks) {
        var onStart = function() {},
            onStop = function() {},
            onMessage = function(msg) {
                $scope.ticks++;
                $scope.$apply();
            };

        $scope.esSupported = !!Ticks;
        $scope.ticks = 0;
        $scope.statusMsg = "Ticks not started";
        $scope.ticksStarted = false;

        $scope.startTicks = function() {
            $scope.statusMsg = "Ticks started";
            $scope.ticksStarted = true;
            Ticks.start(onStart, onStop, onMessage);
        };

        $scope.stopTicks = function() {
            Ticks.stop();
            $scope.statusMsg = "Ticks stopped";
            $scope.ticksStarted = false;
            $scope.ticks = 0;
        };
    })

    .controller("WebSocketSectionCtrl", function($scope, Chat) {
        $scope.wsSupported = !!Chat;
        $scope.joined = false;
        $scope.messages = [];

        $scope.join = function() {
            Chat.join({
                onJoin: function() {
                    $scope.joined = true;
                    $scope.$apply();
                },

                onLeave: function() {
                    $scope.joined = false;
                    $scope.$apply();
                },

                onClose: function() {
                    $scope.joined = false;
                    $scope.$apply();
                },

                onMessage: function(message) {
                    $scope.messages.push(JSON.parse(message.data));
                    $scope.$apply();
                }
            });
        };

        $scope.leave = function() {
            Chat.leave();
        };

        $scope.postMessage = function() {
            Chat.sendMsg($scope.message);
            $scope.message = "";
        };
    });
