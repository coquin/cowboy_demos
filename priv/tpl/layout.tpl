<!DOCTYPE html>
<html ng-app="App">
    <head>
        <title>Webserver</title>
        <meta charset="UTF-8" />

        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" />
        <link rel="stylesheet" type="text/css" href="css/styles.css" />
    </head>
    <body>
        <header class="navbar navbar-static-top navbar-inverse">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Cowboy Demos</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="#auth">Authorization and Session Handling</a></li>
                    <li><a href="#ajax">Ajax Requests</a></li>
                    <li><a href="#eventsource">EventSource</a></li>
                    <li><a href="#websocket">WebSocket</a></li>
                </li>
            </div>
        </header>
        <section class="container">
            <div class="row">
                <div class="col-md-9">
                    {% block content %}{% endblock %}
                </div>
            </div>
        </section>

        <!-- Vendor scripts -->
        <script src="js/vendor/jquery.js"></script>
        <script src="js/vendor/angular.js"></script>
        <script src="js/vendor/angular-sanitize.js"></script>
        <script src="js/vendor/angular-resource.js"></script>

        <!-- App scripts -->
        <script src="js/app.js"></script>
    </body>
</html>
