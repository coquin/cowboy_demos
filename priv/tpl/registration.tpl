{% extends "layout.tpl" %}
{% block content %}
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4 jumbotron">
        <h3>Registration</h3>
        <form>
            <div class="form-group">
                <label for="name_input">Name:</label>
                <input type="text" class="form-control" id="name_input">
            </div>
            <div class="form-group">
                <label for="email_input">Email:</label>
                <input type="email" class="form-control" id="email_input">
            </div>
            <div class="form-group">
                <label for="password_input">Password:</label>
                <input type="password" class="form-control" id="password_input">
            </div>
            <div class="form-group">
                <label for="password_confirm_input">Password again:</label>
                <input type="password" class="form-control" id="password_confirm_input">
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox">Remember me
                </label>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    <div class="col-md-4"></div>
</div>
{% endblock %}
