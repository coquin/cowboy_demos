{% extends "layout.tpl" %}
{% block content %}
<div class="content-section">
    <h1 class="page-header">Overview</h1>
    <p class="lead">Cowboy web server demos. Nothing really serious, just several typical web server tasks solved with Cowboy.</p>
</div>
<div class="content-section">
    <h3 id="auth">Authorization and Session Handling</h3>
    {% comment %}
    <p>You are not logged in.</p>
    <button class="btn btn-primary">Log in</button> or <a href="#register">Register</a>
    {% endcomment %}
    <p class="text-warning">WORK IN PROGRESS.</p>
</div>
<div class="content-section" ng-controller="AjaxSectionCtrl">
    {% verbatim %}
    <h3 id="ajax">AJAX Requests</h3>
    <button class="btn btn-primary" ng-click="loadPost()" ng-if="!post" ng-disabled="isPostLoading" ng-switch="isPostLoading">
        <span ng-switch-when="true">Loading...</span>
        <span ng-switch-default>Load post</span>
    </button>
    <i class="loading" ng-if="isPostLoading"></i>
    <div class="panel panel-default" ng-if="post">
        <div class="panel-heading">
            <h3 class="panel-title">{{post.title}}</h3>
        </div>
        <div class="panel-body">
            <p ng-bind-html="post.body"></p>
        </div>
        <div class="panel-body">
            <h5>Comments</h5>
            <form class="comment-form" ng-submit="postComment()">
                <div class="form-group textarea-form-group">
                    <textarea class="form-control" rows="3" ng-model="$parent.comment" ng-disabled="isCommentPosting"></textarea>
                    <i class="loading" ng-if="isCommentPosting"></i>
                </div>
                <div class="form-group">
                    <button class="btn btn-default" ng-disabled="isCommentPosting" ng-switch="isCommentPosting">
                        <span ng-switch-when="true">Posting...</span>
                        <span ng-switch-default>Post comment</span>
                    </button>
                </div>
            </form>
        </div>
        <ul class="list-group">
            <li class="list-group-item" ng-repeat="comment in post.comments">
                <span ng-class="{'text-muted': comment.author == 'guest', 'text-warning': comment.author != 'guest'}">{{comment.author}}: </span>{{comment.text}}
            </li>
        </ul>
    </div>
    {% endverbatim %}
</div>
<div class="content-section" ng-controller="EventSourceSectionCtrl">
    {% verbatim %}
    <h3 id="eventsource">EventSource</h3>
    <p class="text-danger" ng-if="!esSupported">Sorry, your browser doesn't support EventSource API.</p>
    <div ng-if="esSupported">
        <span>Ticks received: <span class="badge">{{ticks}}</span></span>&nbsp;&nbsp;&nbsp;<button class="btn btn-default" ng-click="startTicks()" ng-if="!ticksStarted">Start ticks</button><button class="btn btn-default" ng-click="stopTicks()" ng-if="ticksStarted">Stop ticks</button>
        <p class="text-info">{{statusMsg}}</p>
    </div>
    {% endverbatim %}
</div>
<div class="content-section" ng-controller="WebSocketSectionCtrl">
    {% verbatim %}
    <h3 id="websocket">WebSocket</h3>
    <p>Simple chat room implementation with WebSockets. Connect, open this page in another browser/tab and play with it</p>
    <p class="text-danger" ng-if="!wsSupported">Sorry, your browser doesn't support WebSocket API.</p>
    <div ng-if="wsSupported">
        <button class="btn btn-default" ng-click="join()" ng-if="!joined">Join chat room</button>
        <button class="btn btn-default" ng-click="leave()" ng-if="joined">Leave chat room</button>
        <br><br>
        <div class="panel panel-default chat-room-panel">
            <div class="panel-heading">
                <h3 class="panel-title">Chat room</h3>
            </div>
            <div class="panel-body">
                <ul class="messages-list" ng-repeat="message in messages">
                    <li ng-class="{ 'text-muted': message.type == 'notify' }">
                        <strong>{{message.user}}<span ng-if="message.type == 'message'">:</span></strong> {{message.message}}
                    </li>
                </ul>
            </div>
        </div>
        <form class="form-horizontal" ng-submit="postMessage()">
            <div class="form-group">
                <label for="message" class="col-sm-2 control-label">Say smthng:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="message" placeholder="Your message" ng-model="$parent.message" ng-disabled="!$parent.joined">
                </div>
                <div class="col-sm-1">
                    <button class="btn btn-primary" ng-disabled="!$parent.joined">Say</button>
                </div>
            </div>
        </form>
    </div>
    {% endverbatim %}
</div>
{% endblock %}
