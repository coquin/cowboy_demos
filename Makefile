REBAR = `which rebar`
CURDIR = `pwd`

all: deps compile

deps:
	@( $(REBAR) get-deps )

compile: clean
	@( $(REBAR) compile )

clean:
	@( $(REBAR) clean )

run:
	@( erl -pa $(CURDIR)/ebin deps/*/ebin -s cowboy_demos )

.PHONY: all deps compile clean run
